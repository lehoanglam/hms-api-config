module.exports = {
    "apps": [{
        "name": "api",
        "script": `/code/source/index.js`,
        "instances": 4,
        "exec_mode": "cluster",
        "merge_logs": true,
        "env": {
            "NODE_ENV": "development",
            "TZ": "UTC"
        }
    }]
}
